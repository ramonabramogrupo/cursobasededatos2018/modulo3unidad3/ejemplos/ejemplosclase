/*
  EJEMPLOS DE BUCLES
*/
USE procedimientosfunciones1;

-- CREO UNA TABLA PARA PRUEBAS

CREATE OR REPLACE TABLE pruebas(
  ID int AUTO_INCREMENT,
  numeros int DEFAULT 0,
  PRIMARY KEY (id)
  );

/**
-- procedimiento almacenado bucle1
-- ARGUMENTOS : A1 entero, A2 entero
-- INTRODUCIR DATOS EN LA TABLA PRUEBAS DESDE EL ARGUMENTO1 
-- HASTA EL ARGUMENTO2
-- NO DEBE INTRODUCIR NI EL 8 NI EL 12
**/


-- VERSION CON ITERATE

DELIMITER //
CREATE OR REPLACE PROCEDURE bucle1 (IN a1 int, IN a2 int)
BEGIN
  DECLARE contador int DEFAULT a1; -- variable contador

  -- comienzo del bucle etiqueta
  etiqueta: WHILE (contador<=a2) DO 

    IF (contador=8 OR contador=12) THEN
      SET contador=contador+1;
      ITERATE etiqueta;
    END IF;

    -- meter el dato en la tabla
    INSERT INTO pruebas VALUE(DEFAULT, contador);

    -- actualizar el contador
    set contador=contador+1;

  END WHILE etiqueta;
  -- fin del bucle etiqueta

END //
DELIMITER ;

-- para probar el procedimiento
CALL bucle1(5,20);
SELECT * FROM pruebas;

-- VERSION SIN ITERATE
DELIMITER //
CREATE OR REPLACE PROCEDURE bucle2 (IN a1 int, IN a2 int)
BEGIN
  DECLARE contador int DEFAULT a1; -- variable contador

  -- comienzo del bucle etiqueta
  WHILE (contador<=a2) DO 
    IF (contador<>8 AND contador<>12) THEN
      -- meter el dato en la tabla
      INSERT INTO pruebas VALUE(DEFAULT, contador);   
    END IF;

    -- actualizar el contador
    set contador=contador+1;

  END WHILE;
  -- fin del bucle etiqueta

END //
DELIMITER ;

-- para probar el procedimiento
CALL bucle2(5,20);
SELECT * FROM pruebas;


/**
  -- FUNCION1
  -- ARGUMENTOS : N1, N2, N3
  -- CREAR UNA FUNCION QUE LE PASEMOS 3 NUMEROS Y TE
  -- DEVUELVA LA SUMA DE ELLOS
  -- SI LOS NUMEROS NO SON MAYORES QUE 0 DEVUELVE NULL
**/

DELIMITER //
CREATE OR REPLACE FUNCTION funcion1(n1 int, n2 int, n3 int)
RETURNS int
BEGIN
  DECLARE suma int DEFAULT NULL;
  
  -- analizas los datos y preparas el valor a devolver
  IF(n1>0 AND n2>0 AND n3>0) THEN
    SET suma=n1+n2+n3;
  END IF;
  
  -- al final realizo el return
  RETURN suma;
END //
DELIMITER ;

SELECT funcion1(23,34,2);


DELIMITER //
CREATE OR REPLACE FUNCTION funcion1a(n1 int, n2 int, n3 int)
RETURNS int
BEGIN
  DECLARE suma int DEFAULT NULL;
  IF(n1<=0 OR n2<=0 OR n3<=0) THEN
    RETURN suma;
  END IF;
  
  set suma=n1+n2+n3;
  RETURN suma;
END //
DELIMITER ;
